#!/bin/bash
set -e

#echo "Running jshint checks."
#jshint -c jshint.rc src/polypeach.js src/polypeach-worker.js

#echo "Running eslint checks."
#eslint src/polypeach.ts src/polypeach-worker.ts

echo "Running tsc checks."
tsc --build tsconfig.json
