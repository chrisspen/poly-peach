#!/bin/bash
npm install eslint --save-dev
npm install eslint-plugin-dollar-sign@latest --save-dev
npm install eslint-plugin-jquery@latest --save-dev
npm install jshint --save-dev
npm install typescript --save-dev
npm install @types/node --save-dev
npm install tone@14.7.77 --save-dev
npm install webworkify@1.5.0 --save-dev
npm install mocha --save-dev
npm install fft.js --save-dev
#npm install -g typescript-formatter
