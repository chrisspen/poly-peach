
const FFT = require('fft.js');

const TOTAL_NOTES = 12;

const DEFAULT_REFERENCE_FREQUENCY = 130.81278265; // C3
const DEFAULT_HARMONICS = 2;
const DEFAULT_OCTAVES = 4;
const DEFAULT_BINS = 2;

class Chromagram {

    private window:Array<number>;
    private buffer:Array<number>;
    private magnitudeSpectrum:Array<number>;
    private downsampledInputAudioFrame:Array<number>;
    private chromagram:Array<number>;
    private pitches:Array<number>;

    private referenceFrequency: number;
    private noteFrequencies: Array<number>;

    private bufferSize:number = 8192;
    private samplingFrequency: number;
    private inputAudioFrameSize: number;
    private downSampledAudioFrameSize: number;
    private numHarmonics: number;
    private numOctaves: number;
    private numBinsToSearch: number;
    private numSamplesSinceLastCalculation: number;
    private chromaCalculationInterval: number;
    private chromaReady: boolean;

    private kiss_ready: boolean = false;
    private fft: typeof FFT;
    private realInput: Array<number>;
    private complexOutput: Array<number>;

    constructor(frameSize: number, fs: number){
        this.setSamplingFrequency(fs);
        this.setInputAudioFrameSize(frameSize);
        this.setParameters(DEFAULT_REFERENCE_FREQUENCY, DEFAULT_HARMONICS, DEFAULT_OCTAVES, DEFAULT_BINS);
    }

    setParameters(_referenceFrequency: number, _numHarmonics: number, _numOctaves: number, _numBinsToSearch: number){

        this.setReferenceFrequency(_referenceFrequency);
        this.setNumHarmonics(_numHarmonics);
        this.setNumOctaves(_numOctaves);
        this.setNumBinsToSearch(_numBinsToSearch);

        this.noteFrequencies = new Array(TOTAL_NOTES*this.numOctaves);
        for (let i = 0; i < this.noteFrequencies.length; i++){
            this.noteFrequencies[i] = this.referenceFrequency * Math.pow(2, ((i) / TOTAL_NOTES));
        }

        this.setupFFT();

        this.buffer = new Array(this.bufferSize);
        this.buffer.fill(0);

        this.realInput = new Array(this.bufferSize);
        this.realInput.fill(0);

        this.chromagram = new Array(TOTAL_NOTES);
        this.chromagram.fill(0);

        this.pitches = new Array(TOTAL_NOTES*this.numOctaves);
        this.pitches.fill(0);

        for (let i = 0; i < this.chromagram.length; i++){
            this.chromagram[i] = 0.0;
        }

        for (let i = 0; i < this.pitches.length; i++){
            this.pitches[i] = 0.0;
        }

        this.magnitudeSpectrum = new Array((this.bufferSize/2)+1);
        this.magnitudeSpectrum.fill(0);

        this.makeHammingWindow();

        this.numSamplesSinceLastCalculation = 0;

        this.chromaCalculationInterval = 4096;

        this.chromaReady = false;
    }

    processAudioFrame(inputAudioFrame: Array<number>){
        this.chromaReady = false;

        this.downSampleFrame(inputAudioFrame);

        for (let i = 0; i < this.bufferSize - this.downSampledAudioFrameSize; i++){
            this.buffer[i] = this.buffer[i + this.downSampledAudioFrameSize];
        }

        let n = 0;
        for (let i = (this.bufferSize - this.downSampledAudioFrameSize); i < this.bufferSize; i++){
            this.buffer[i] = this.downsampledInputAudioFrame[n];
            n++;
        }

        this.numSamplesSinceLastCalculation += this.inputAudioFrameSize;

        if (this.numSamplesSinceLastCalculation >= this.chromaCalculationInterval){
            this.calculateChromagram();
            this.numSamplesSinceLastCalculation = 0;
        }

    }

    setInputAudioFrameSize(frameSize: number)
    {
        this.inputAudioFrameSize = frameSize;
        this.downsampledInputAudioFrame = new Array(this.inputAudioFrameSize / 4);
        this.downsampledInputAudioFrame.fill(0);
        this.downSampledAudioFrameSize = this.downsampledInputAudioFrame.length;
    }

    setReferenceFrequency (freq: number)
    {
        this.referenceFrequency = freq;
    }

    getReferenceFrequency ()
    {
        return this.referenceFrequency;
    }

    setNumHarmonics (n: number)
    {
        this.numHarmonics = n;
    }

    setNumOctaves (n: number)
    {
        this.numOctaves = n;
    }

    getNumOctaves ()
    {
        return this.numOctaves;
    }

    setNumBinsToSearch (n: number)
    {
        this.numBinsToSearch = n;
    }

    setSamplingFrequency (fs: number)
    {
        this.samplingFrequency = fs;
    }

    setChromaCalculationInterval (numSamples: number)
    {
        this.chromaCalculationInterval = numSamples;
    }

    getChromagram()
    {
        return this.chromagram;
    }

    getPitches()
    {
        return this.pitches;
    }

    isReady()
    {
        return this.chromaReady;
    }

    setupFFT(){
        this.fft = new FFT(this.bufferSize);
        this.complexOutput = this.fft.createComplexArray();
        this.kiss_ready = true;
    }

    calculateChromagram(){
        this.calculateMagnitudeSpectrum();

        let divisorRatio: number = ((this.samplingFrequency) / 4.0) / (this.bufferSize);

        for (let n = 0; n < TOTAL_NOTES; n++) {
            let chromaSum = 0.0;

            for (let octave = 1; octave <= this.numOctaves; octave++) {
                let noteSum = 0.0;
                let centerBin = this.round((this.noteFrequencies[n + (octave-1)*TOTAL_NOTES]) / divisorRatio);
                let minBin = centerBin - (this.numBinsToSearch);
                let maxBin = centerBin + (this.numBinsToSearch);
                let maxVal = 0.0;
                for (let k = minBin; k < maxBin; k++)
                {
                    if (this.magnitudeSpectrum[k] > maxVal)
                    {
                        maxVal = this.magnitudeSpectrum[k];
                    }
                }
                noteSum += maxVal;
                this.pitches[n + (octave-1)*TOTAL_NOTES] = noteSum;

                //TODO:deprecated?
                noteSum = 0.0;
                for (let harmonic = 1; harmonic <= this.numHarmonics; harmonic++)
                {
                    let centerBin = this.round((this.noteFrequencies[n + (octave-1)*TOTAL_NOTES] * octave * harmonic) / divisorRatio);
                    let minBin = centerBin - (this.numBinsToSearch * harmonic);
                    let maxBin = centerBin + (this.numBinsToSearch * harmonic);

                    let maxVal = 0.0;

                    for (let k = minBin; k < maxBin; k++)
                    {
                        if (this.magnitudeSpectrum[k] > maxVal)
                        {
                            maxVal = this.magnitudeSpectrum[k];
                        }
                    }

                    noteSum += (maxVal / harmonic);
                }
                chromaSum += noteSum;
            }

            this.chromagram[n] = chromaSum;
        }

        this.chromaReady = true;
    }

    calculateMagnitudeSpectrum(){
        let i = 0;

        for (let i = 0;i < this.bufferSize; i++)
        {
            this.realInput[i] = this.buffer[i] * this.window[i];
        }

        // This is where all the magic happens.
        this.fft.realTransform(this.complexOutput, this.realInput);
        this.fft.completeSpectrum(this.complexOutput);//TODO:is this unnecessary?

        for (let i = 0; i < (this.bufferSize / 2) + 1; i++)
        {
            this.magnitudeSpectrum[i] = Math.sqrt(Math.pow(this.complexOutput[i*2], 2) + Math.pow(this.complexOutput[i*2+1], 2));
            this.magnitudeSpectrum[i] = Math.sqrt(this.magnitudeSpectrum[i]);
        }
    }

    downSampleFrame (inputAudioFrame: Array<number>)
    {
        let filteredFrame = new Array(this.inputAudioFrameSize);
        filteredFrame.fill(0);

        let b0,b1,b2,a1,a2: number;
        let x_1,x_2,y_1,y_2: number;

        b0 = 0.2929;
        b1 = 0.5858;
        b2 = 0.2929;
        a1 = -0.0000;
        a2 = 0.1716;

        x_1 = 0;
        x_2 = 0;
        y_1 = 0;
        y_2 = 0;

        for (let i = 0; i < this.inputAudioFrameSize; i++)
        {
            filteredFrame[i] = inputAudioFrame[i] * b0 + x_1 * b1 + x_2 * b2 - y_1 * a1 - y_2 * a2;

            x_2 = x_1;
            x_1 = inputAudioFrame[i];
            y_2 = y_1;
            y_1 = filteredFrame[i];
        }

        for (let i = 0; i < this.inputAudioFrameSize / 4; i++)
        {
            this.downsampledInputAudioFrame[i] = filteredFrame[i * 4];
        }
    }

    makeHammingWindow()
    {
        this.window = new Array(this.bufferSize);
        this.window.fill(0);
        for (let n = 0; n < this.bufferSize; n++)
        {
            this.window[n] = 0.54 - 0.46 * Math.cos (2 * Math.PI * (n / (this.bufferSize)));
        }
    }

    round(val: number){
        return Math.floor(val + 0.5);
    }

}

export {
    Chromagram
};
