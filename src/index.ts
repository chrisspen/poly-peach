import { PolyPeachDetector } from './polypeach';
import { Chromagram } from './chromagram';

export {
    PolyPeachDetector,
    Chromagram
};

export default {
    PolyPeachDetector,
    Chromagram
};
