import { PolyPeachDetector } from './polypeach';
import { Chromagram } from './chromagram';
export { PolyPeachDetector, Chromagram };
declare const _default: {
    PolyPeachDetector: typeof PolyPeachDetector;
    Chromagram: typeof Chromagram;
};
export default _default;
