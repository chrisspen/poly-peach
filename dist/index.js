"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Chromagram = exports.PolyPeachDetector = void 0;
var polypeach_1 = require("./polypeach");
Object.defineProperty(exports, "PolyPeachDetector", { enumerable: true, get: function () { return polypeach_1.PolyPeachDetector; } });
var chromagram_1 = require("./chromagram");
Object.defineProperty(exports, "Chromagram", { enumerable: true, get: function () { return chromagram_1.Chromagram; } });
exports.default = {
    PolyPeachDetector: polypeach_1.PolyPeachDetector,
    Chromagram: chromagram_1.Chromagram
};
