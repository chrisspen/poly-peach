#!/bin/bash
# https://www.npmjs.com/package/js-beautify
#js-beautify --config=jsbeautify.config --replace --type=js src/polypeach-worker.js src/polypeach.js src/chords/chromagram.js
# https://www.npmjs.com/package/typescript-formatter
tsfmt --replace src/polypeach.ts src/polypeach-worker.ts src/index.ts
