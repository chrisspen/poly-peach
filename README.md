[![Pipeline Status](https://gitlab.com/chrisspen/poly-peach/badges/master/pipeline.svg)](https://gitlab.com/chrisspen/poly-peach/commits/master)

# Poly Peach

A targeted [polyphonic](https://en.wikipedia.org/wiki/Polyphony) pitch detection library for Javascript.

Given a set of specific [musical notes](https://en.wikipedia.org/wiki/Musical_note) to listen for, this algorithm
detects when they're present in an audio sample.

# Implementation

The algorithm works by calculating the FFT of an audio sample and aggregating
the signal strengths for frequency bandwidths of each musical note.

Then, for each note it's listening for, it calculates the notes corresponding to the first N harmonics,
and compares an expected signature for these notes to the sample.

If the error is below a given threshold, then the note is reported as found.

# Development

The package is structured in three parts:

1. The C++ code that quickly calculates the Fast Fourier Transformation
to construct musical note frequency spectrum.
2. The Emscripten wrapper to convert the C++ code to WASM.
3. The Node.js code to present a high-level interface.

Build:

To compile the C++ and WASM wrapper, run:

    ./build.sh

Run tests:

    npm run test

or:

    mocha

    mocha -g 'test name'

To publish:

    tsc
    npm login
    npm publish
