#!/bin/bash
virtualenv -p python3.9 .env
. .env/bin/activate
pip install -r pip-requirements.txt
