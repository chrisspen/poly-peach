#!/bin/bash
. .env/bin/activate
export FLASK_DEBUG=1
#flask run --host=0.0.0.0 --cert=adhoc
flask run --host=0.0.0.0 --port=6161 --cert=ssl/example.crt --key=ssl/example.key
