import os
import time
import csv
import re
import logging
import uuid
from datetime import datetime

from flask import Flask, render_template, send_from_directory, jsonify

app = Flask(__name__, static_url_path='', template_folder='templates')

logging.basicConfig()
logging.root.setLevel(logging.NOTSET)
logging.basicConfig(level=logging.NOTSET)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

@app.route('/', defaults={'path': 'index.html'})
def index(path):
    return render_template(path, rand_tag='?%s' % time.time())

@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)

if __name__ == "__main__":
    app.run()
